package ru.omsu.imit.multipleactivities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_second.*

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)

        val user = intent.extras?.get("username")?.toString() ?: "Даша"
        val gift = intent.extras?.get("gift")?.toString() ?: "апельсин"
        val sender = intent.extras?.get("sender")?.toString() ?: "Кирилл"

        messageText.text = "user ${user}, ${sender} передал Вам в подарок ${gift}"
    }
}
